package internal

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"hash"
	"io"
	"io/fs"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/riton/ci-dir-release-cli/versionutils"
)

type AppOptions struct {
	AddChecksumFile    bool
	PackageName        string
	PackageVersion     string
	PackageRegistryURL string
}

type app struct {
	opts          *AppOptions
	newHasherFunc func() hash.Hash
	cksumFile     checksumFile
	gclient       *gClient
}

func NewApp(opts *AppOptions) *app {

	gclient, err := newWrappedGitlabClientWithOptions(newGitlabClientOptionsFromCIJob())
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"error": err,
		}).Fatal("fail to create gitlab client")
	}

	return &app{
		opts:          opts,
		newHasherFunc: sha256.New,
		gclient:       gclient,
		cksumFile:     newChecksumFile(),
	}
}

func (a *app) Run(baseDir string) error {
	releaseID, err := a.createRelease()
	if err != nil {
		return errors.Wrap(err, "creating release")
	}

	err = filepath.WalkDir(baseDir, func(path string, d fs.DirEntry, _ error) error {
		if path == baseDir {
			return nil
		}

		if d.IsDir() {
			return fmt.Errorf("no subdirectory is allowed in %s", baseDir)
		}

		logrus.WithFields(logrus.Fields{
			"name": d.Name(),
		}).Debug("processing file")

		hasher := a.newHasherFunc()

		fd, err := os.Open(path)
		if err != nil {
			return errors.Wrap(err, "opening file for reading")
		}
		defer fd.Close()

		hReader := io.TeeReader(fd, hasher)

		uploadOpts := ProjectPackageUploadFileOptions{
			ProjectID:      getProjectIDFromCIJob(),
			PackageType:    "generic",
			PackageName:    a.opts.PackageName,
			PackageVersion: versionutils.StripVersionPrefix(a.opts.PackageVersion, "v"),
			Filename:       d.Name(),
			Hidden:         false,
		}
		packageFileURL, err := a.gclient.ProjectPackageUploadFile(hReader, uploadOpts)
		if err != nil {
			return errors.Wrapf(err, "uploading package file %s", d.Name())
		}

		logrus.WithFields(logrus.Fields{
			"name":             d.Name(),
			"package-file-url": packageFileURL,
		}).Debug("file uploaded to package repository")

		name := d.Name()
		filename := "/" + name
		releaseLink, _, err := a.gclient.ReleaseLinks.CreateReleaseLink(
			getProjectIDFromCIJob(),
			releaseID,
			&gitlab.CreateReleaseLinkOptions{
				Name:     &name,
				URL:      &packageFileURL,
				FilePath: &filename,
			})
		if err != nil {
			return errors.Wrapf(err, "attaching %s to release %s", name, releaseID)
		}

		logrus.WithFields(logrus.Fields{
			"name":             d.Name(),
			"package-file-url": packageFileURL,
			"release-link":     releaseLink,
		}).Debug("file processed")

		a.cksumFile[d.Name()] = hex.EncodeToString(hasher.Sum(nil))

		return nil
	})
	if err != nil {
		return err
	}

	if a.opts.AddChecksumFile {
		packageChecksumFilename, err := a.uploadChecksumFile()
		if err != nil {
			return errors.Wrap(err, "uploading checksum file")
		}
		logrus.WithFields(logrus.Fields{
			"checksum-file-url": packageChecksumFilename,
		}).Debug("checksum file uploaded to package repository")

		name := ChecksumFilename
		filename := "/" + name
		releaseLink, _, err := a.gclient.ReleaseLinks.CreateReleaseLink(
			getProjectIDFromCIJob(),
			releaseID,
			&gitlab.CreateReleaseLinkOptions{
				Name:     &name,
				URL:      &packageChecksumFilename,
				FilePath: &filename,
			})
		if err != nil {
			return errors.Wrapf(err, "attaching %s to release %s", name, releaseID)
		}

		logrus.WithFields(logrus.Fields{
			"checksum-file-url": packageChecksumFilename,
			"release-link":      releaseLink,
		}).Debug("checksum file attached to release")
	}

	return nil
}

func (a *app) createRelease() (string, error) {
	name := "Release " + a.opts.PackageVersion
	ref := getCurrentCommitFromCIJob()
	description := "" // TODO:

	_, _, err := a.gclient.Releases.CreateRelease(getProjectIDFromCIJob(), &gitlab.CreateReleaseOptions{
		Name:        &name,
		Description: &description,
		Ref:         &ref,
		TagName:     &name,
	})
	if err != nil {
		return "", err
	}

	// The ID of the release seems to be its name
	return name, nil
}
