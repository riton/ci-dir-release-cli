package internal

import (
	"strings"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/riton/ci-dir-release-cli/versionutils"
)

const (
	ChecksumFilename = "checksums.txt"
)

func (a *app) uploadChecksumFile() (string, error) {
	cksumFileContent := a.cksumFile.String()

	logrus.WithFields(logrus.Fields{
		"name": ChecksumFilename,
	}).Debug("uploading checksum file")

	uploadOpts := ProjectPackageUploadFileOptions{
		ProjectID:      getProjectIDFromCIJob(),
		PackageType:    "generic",
		PackageName:    a.opts.PackageName,
		PackageVersion: versionutils.StripVersionPrefix(a.opts.PackageVersion, "v"),
		Filename:       ChecksumFilename,
		Hidden:         false,
	}
	packageCksumFilename, err := a.gclient.ProjectPackageUploadFile(strings.NewReader(cksumFileContent), uploadOpts)
	if err != nil {
		return "", errors.Wrapf(err, "uploading package file %s", ChecksumFilename)
	}

	logrus.WithFields(logrus.Fields{
		"name": ChecksumFilename,
	}).Debug("checksum file uploaded")

	return packageCksumFilename, nil
}
