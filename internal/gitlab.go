package internal

import (
	"io"
	"net/http"

	"github.com/hashicorp/go-retryablehttp"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

type gitlabClientOptions struct {
	APIEndpoint string
	AccessToken string
}

func newGitlabClientWithOptions(opts *gitlabClientOptions) (*gitlab.Client, error) {
	glClientCfg := []gitlab.ClientOptionFunc{
		gitlab.WithBaseURL(opts.APIEndpoint),
	}
	return gitlab.NewJobClient(opts.AccessToken, glClientCfg...)
}

type gClient struct {
	*gitlab.Client
}

func newWrappedGitlabClientWithOptions(opts *gitlabClientOptions) (*gClient, error) {
	gclient, err := newGitlabClientWithOptions(opts)
	if err != nil {
		return nil, err
	}
	return &gClient{gclient}, nil
}

// PUT /projects/:id/packages/generic/:package_name/:package_version/:file_name?status=:status
/*
Attribute
Type
Required
Description

id
integer/string
yes
The ID or URL-encoded path of the project.


package_name
string
yes
The package name. It can contain only lowercase letters (a-z), uppercase letter (A-Z), numbers (0-9), dots (.), hyphens (-), or underscores (_).


package_version
string
yes
The package version. The following regex validates this: \A(\.?[\w\+-]+\.?)+\z. You can test your version strings on Rubular.


file_name
string
yes
The filename. It can contain only lowercase letters (a-z), uppercase letter (A-Z), numbers (0-9), dots (.), hyphens (-), or underscores (_).


status
string
no
The package status. It can be default (default) or hidden. Hidden packages do not appear in the UI or package API list endpoints.
*/
type ProjectPackageUploadFileOptions struct {
	ProjectID      string
	PackageName    string
	PackageVersion string
	PackageType    string // only generic has been tested
	Filename       string
	Hidden         bool
}

func (g *gClient) ProjectPackageUploadFile(r io.Reader, opts ProjectPackageUploadFileOptions) (string, error) {
	packageFilenameBuilderFunc := func(filename string) string {
		return g.BaseURL().String() + "projects/" + opts.ProjectID + "/packages/" + opts.PackageType + "/" +
			opts.PackageName + "/" + opts.PackageVersion + "/" + filename
	}

	endpoint := packageFilenameBuilderFunc(opts.Filename)

	req, err := retryablehttp.NewRequest(http.MethodPut, endpoint, r)
	if err != nil {
		return "", errors.Wrap(err, "creating HTTP request")
	}

	if opts.Hidden {
		qParams := req.URL.Query()
		qParams.Add("status", "hidden")
		req.URL.RawQuery = qParams.Encode()
	}

	_, err = g.Do(req, nil)
	if err != nil {
		return "", errors.Wrap(err, "performing HTTP request")
	}

	logrus.WithFields(logrus.Fields{
		"endpoint": req.URL.String(),
	}).Debug("project package file uploaded")

	return endpoint, nil
}
