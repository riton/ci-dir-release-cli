package internal

import (
	"fmt"
	"strings"
)

type checksumFile map[string]string

func newChecksumFile() checksumFile {
	return checksumFile(make(map[string]string))
}

func (c checksumFile) String() string {
	var sb strings.Builder
	for filename, hash := range c {
		fmt.Fprintf(&sb, "%s\t%s\n", hash, filename)
	}
	return sb.String()
}
