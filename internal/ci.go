package internal

import "os"

// only use environment variables available from C.I job
func newGitlabClientOptionsFromCIJob() *gitlabClientOptions {
	return &gitlabClientOptions{
		APIEndpoint: os.Getenv("CI_API_V4_URL"),
		AccessToken: os.Getenv("CI_JOB_TOKEN"),
	}
}

func getProjectIDFromCIJob() string {
	return os.Getenv("CI_PROJECT_ID")
}

func getCurrentCommitFromCIJob() string {
	return os.Getenv("CI_COMMIT_SHA")
}
