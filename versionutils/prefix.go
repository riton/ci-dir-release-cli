package versionutils

import "strings"

// StripVersionPrefix allow to easily strip a version
// prefix such as "v"
func StripVersionPrefix(v, prefix string) string {
	if strings.HasPrefix(v, prefix) {
		return v[len(prefix):]
	}
	return v
}
