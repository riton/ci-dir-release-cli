/*
Copyright © 2021 Remi Ferrand

Contributor(s): Remi Ferrand <riton.github_at_gmail(dot)com>, 2021

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*/
package cmd

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/riton/ci-dir-release-cli/internal"
	"gitlab.com/riton/ci-dir-release-cli/versionutils"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "ci-dir-release-cli",
	Short: "Helper to release a directory to Gitlab releases and generic packages",
	Long:  ``,
	Args:  cobra.ExactArgs(1),
	PreRun: func(cmd *cobra.Command, args []string) {
		debug, _ := cmd.Flags().GetBool("debug")
		if debug {
			logrus.SetLevel(logrus.DebugLevel)
		}
	},
	Run: appCobraAdapter,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().BoolP("debug", "d", false, "Enable debug")
	rootCmd.Flags().BoolP("checksum", "s", false, "Enable checksum file")
	rootCmd.Flags().String("package-version", "", "Package version to create")
	rootCmd.Flags().String("package-name", "release", "Package name to create")
	rootCmd.MarkFlagRequired("package-version")
}

func appCobraAdapter(cmd *cobra.Command, args []string) {
	packageVersion, _ := cmd.Flags().GetString("package-version")
	packageName, _ := cmd.Flags().GetString("package-name")
	useChecksumFile, _ := cmd.Flags().GetBool("checksum")

	// export PACKAGE_REGISTRY_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/releases/${PACKAGE_VERSION#v}"
	packageRegistryURL := os.ExpandEnv("${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/releases/" + versionutils.StripVersionPrefix(packageVersion, "v"))

	app := internal.NewApp(&internal.AppOptions{
		AddChecksumFile:    useChecksumFile,
		PackageName:        packageName,
		PackageVersion:     packageVersion,
		PackageRegistryURL: packageRegistryURL,
	})

	if err := app.Run(args[0]); err != nil {
		logrus.WithFields(logrus.Fields{
			"error": err,
		}).Fatal("fail to upload or create release")
	}
}
