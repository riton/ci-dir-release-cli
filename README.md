# ci-dir-release-cli

[![pipeline status](https://gitlab.com/riton/ci-dir-release-cli/badges/develop/pipeline.svg)](https://gitlab.com/riton/ci-dir-release-cli/-/commits/develop)

C.I CLI Helper to release all assets of a directory to gitlab generic packages and release page.

This CLI is meant to be run from a gitlab-ci job.

It will upload all of your assets to the _generic package_ repository of your project, and finally create a release that references those packages.

**Important**: `ASSETS_DIRECTORY` can't contain sub-directories.

## Usage

```
Helper to release a directory to Gitlab releases and generic packages

Usage:
  ci-dir-release-cli [flags] ASSETS_DIRECTORY

Flags:
  -s, --checksum                 Enable checksum file
  -d, --debug                    Enable debug
  -h, --help                     help for ci-dir-release-cli
      --package-name string      Package name to create (default "release")
      --package-version string   Package version to create
```

## Example

### From a C.I job

TODO

```
```

### Manual call

```
$ CI_COMMIT_SHA="XXXX" CI_API_V4_URL="https://gitlab.com/api/v4" CI_PROJECT_ID="42" CI_JOB_TOKEN="s3cr3t" ci-dir-release-cli --package-version v1.2.8 --checksum /tmp/testdata
```
